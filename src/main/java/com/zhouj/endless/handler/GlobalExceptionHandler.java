package com.zhouj.endless.handler;

import com.zhouj.endless.response.ResultBody;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 简单的统一异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = RuntimeException.class)
    public ResultBody handleException(RuntimeException exception) {
        ResultBody result = new ResultBody(exception);
        return result;
    }
}
