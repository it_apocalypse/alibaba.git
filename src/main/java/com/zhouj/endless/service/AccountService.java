package com.zhouj.endless.service;


import com.zhouj.endless.model.Account;

import java.util.List;

public interface AccountService {
    List<Account> listAccount();
}
