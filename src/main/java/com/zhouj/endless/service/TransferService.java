package com.zhouj.endless.service;

import com.zhouj.endless.dto.TransferRequestParamDTO;
import com.zhouj.endless.model.Transfer;

public interface TransferService {

    /**
     * 转账接口
     *
     * @param paramDTO 转账请求参数
     * @return Transfer
     */
    Transfer transfer(TransferRequestParamDTO paramDTO);
}
